const fs = require('fs')
const path = require('path')
const file = fs.readFileSync(path.resolve(__dirname, 'input', 'day6.txt'))

const lines = file.toString().split('\n')

const part1 = (lines) => {
    // figure out the edges
    // and initialize queue
    let leftBound, rightBound, topBound, bottomBound
    let queue = new Array()
    for (const i in lines) {
        const line = lines[i]
        const parts = line.split(',')
        const x = parseInt(parts[0])
        const y = parseInt(parts[1])

        queue.push({
            x: x,
            y: y,
            id: i
        })

        if (x <= leftBound || leftBound === undefined) leftBound = x - 1
        else if (x >= rightBound || rightBound === undefined) rightBound = x + 1

        if (y <= topBound || topBound === undefined) topBound = y - 1
        else if (y >= bottomBound || bottomBound === undefined) bottomBound = y + 1
    }

    const bounds = {
        left: leftBound,
        right: rightBound,
        top: topBound,
        bottom: bottomBound
    }

    const map = {}
    const idToArea = {}
    let distance = 0
    let nextRound = new Array()

    while (queue.length > 0) {

        while (queue.length > 0) {
            const coordinate = queue.shift()

            if (!idToArea[coordinate.id]) idToArea[coordinate.id] = 0
    
            expand(map, idToArea, nextRound, coordinate.id, coordinate.x, coordinate.y, distance, bounds)
        }

        queue = nextRound
        nextRound = new Array()
        distance++
    }

    // any area that expands to the bounds
    // is infinite, so clear those out
    for (let i = bounds.left; i < bounds.right; i++) {
        const j = bounds.top
        const cell = map[i][j]
        delete idToArea[cell.id]
    }
    for (let j = bounds.top; j < bounds.bottom; j++) {
        const i = bounds.right
        const cell = map[i][j]
        delete idToArea[cell.id]
    }
    for (let i = bounds.right; i > bounds.left; i--) {
        const j = bounds.bottom
        const cell = map[i][j]
        delete idToArea[cell.id]
    }
    for (let j = bounds.bottom; j > bounds.top; j--) {
        const i = bounds.left
        const cell = map[i][j]
        delete idToArea[cell.id]
    }

    let largestArea = 0
    for (const id in idToArea) {
        const area = idToArea[id]
        largestArea = Math.max(area, largestArea)
    }

    return largestArea
}

const expand = (map, idToArea, queue, id, x, y, distance, bounds) => {
    if (!map[x]) map[x] = {}

    if (!map[x][y]) {
        // unvisited
        map[x][y] = {
            id: id,
            distance: distance
        }

        if (inBounds(x+1, y, bounds)) queue.push({x: x+1, y: y, id: id})
        if (inBounds(x-1, y, bounds)) queue.push({x: x-1, y: y, id: id})
        if (inBounds(x, y+1, bounds)) queue.push({x: x, y: y+1, id: id})
        if (inBounds(x, y-1, bounds)) queue.push({x: x, y: y-1, id: id})

        idToArea[id] = idToArea[id] + 1
    }
    else if (map[x][y].distance === distance && map[x][y].id !== id) {
        // visited by another id in same 'growth' cycle
        // aka it is a tie
        const lastId = map[x][y].id
        if (lastId > -1) idToArea[lastId] = idToArea[lastId] - 1
        map[x][y].id = -1
    }
}

const inBounds = (x, y, bounds) => {
    if (x < bounds.left) return false
    else if (x > bounds.right) return false
    else if (y < bounds.top) return false
    else if (y > bounds.bottom) return false
    else return true
}

const part2 = (lines) => {
    let leftBound, rightBound, topBound, bottomBound

    for (const i in lines) {
        const line = lines[i]
        const parts = line.split(',')
        const x = parseInt(parts[0])
        const y = parseInt(parts[1])

        if (x < leftBound || leftBound === undefined) leftBound = x
        else if (x > rightBound || rightBound === undefined) rightBound = x

        if (y < topBound || topBound === undefined) topBound = y
        else if (y > bottomBound || bottomBound === undefined) bottomBound = y
    }

    let regionSize = 0 

    for (let i = leftBound; i <= rightBound; i++ ) {
        for (let j = topBound; j <= bottomBound; j++) {
            let sum = 0
            const maxDistance = 10000
            for (const k in lines) {
                const line = lines[k]
                const parts = line.split(',')
                const x = parseInt(parts[0])
                const y = parseInt(parts[1])
                const distance = Math.abs(x - i) + Math.abs(y - j)
                sum += distance
            }

            if (sum < maxDistance) regionSize++
        }
    }

    return regionSize
}

console.log('Day 6')
console.log(`  Part 1 Solution - ${part1(lines)}`)
console.log(`  Part 2 Solution - ${part2(lines)}`)