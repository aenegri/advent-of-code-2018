const readline = require('readline')
const fs = require('fs')
const path = require('path')

/**
 * Stream file line by line to
 * minimize memory footprint
 * 
 */
const part1 = (filename, callback) => {
    let sum = 0

    const rl = readline.createInterface({
        input: fs.createReadStream(filename),
        crlfDelay: Infinity
    })

    rl.on('line', (line) => {
        const absoluteValue = parseInt(line.substr(1))
        if (line.charAt(0) === '+') {
            sum += absoluteValue
        }
        else if (line.charAt(0) === '-') {
            sum -= absoluteValue
        }
    })

    rl.on('close', () => {
        callback(sum)
    })
}

/**
 * Load into memory and iterate.
 * Not streaming since readline
 * interface does not have a
 * convenient way to loop.
 * 
 */
const part2 = (filename) => {
    let sum = 0
    const fileContents = fs.readFileSync(filename)
    const lines = fileContents.toString().split('\n')
    const seenFrequencies = new Array()
    seenFrequencies[0] = true
    
    while (true) {
        for (const i in lines) {
            const line = lines[i]
            const absoluteValue = parseInt(line.substr(1))
            if (line.charAt(0) === '+') {
                sum += absoluteValue
            }
            else if (line.charAt(0) === '-') {
                sum -= absoluteValue
            }

            if (seenFrequencies[sum]) {
                return sum
            }
            else {
                seenFrequencies[sum] = true
            }
        }
    }
}

const input = path.resolve(__dirname, 'input', 'day1.txt')

part1(input, (sum) => {
    console.log('Day 1')
    console.log(`  Part 1 Solution - ${sum}`)
    console.log(`  Part 2 Solution - ${part2(input)}`)
})



