const fs = require('fs')
const path = require('path')

const file = fs.readFileSync(path.resolve(__dirname, 'input', 'day10.txt'))
const lines = file.toString().split('\n')

class Light {
    constructor(x, y, xVelocity, yVelocity) {
        this.x = x
        this.y = y
        this.xVelocity = xVelocity
        this.yVelocity = yVelocity
    }

    travel() {
        this.x += this.xVelocity
        this.y += this.yVelocity
    }
}

class Sky {
    constructor(lights) {
        this.lights = lights
        this.moveCount = 0

        let minX = this.lights[0].x
        let maxX = this.lights[0].x
        let minY = this.lights[0].y
        let maxY = this.lights[0].y

        this.lights.forEach(light => {
            minX = Math.min(light.x, minX)
            maxX = Math.max(light.x, maxX)
            minY = Math.min(light.y, minY)
            maxY = Math.max(light.y, maxY)
        })

        this.minX = minX + 52000
        this.maxX = maxX - 52000
        this.minY = minY + 52000
        this.maxY = maxY - 52000

        this.xScale = 2000 / (maxX - minX)
        this.yScale = 3000 / (maxY - minY)
    }

    fastForward() {
        const grid = new Array()

        let xMin = this.lights[0].x
        let xMax = this.lights[0].x
        let yMin = this.lights[0].y
        let yMax = this.lights[0].y

        this.lights.forEach(light =>  {
            light.travel()

            xMin = Math.min(light.x, xMin)
            xMax = Math.max(light.x, xMax)
            yMin = Math.min(light.y, yMin)
            yMax = Math.max(light.y, yMax)

            if (!grid[light.x]) grid[light.x] = new Array()
            grid[light.x][light.y] = true
        })

        this.moveCount++
        
        // only show sky when output is tightened up a bit
        if (xMax - xMin < 100) this.print(grid, xMin, xMax, yMin, yMax)
    }

    print(grid, xMin, xMax, yMin, yMax) {

        console.log(`~~~~~~~~~~Start - ${this.moveCount} seconds elapsed`)

        for (let j = yMin; j < yMax; j++) {
            let line = '['
            for (let i = xMin; i < xMax; i++) {
                line += grid[i] && grid[i][j] ? '#' : '.'
            }
            line += ']'
            console.log(line)
        }

        console.log('~~~~~~~~~~End')
    }
}

const part1 = (sky) => {
    const maxTime = 20000
    let time = 0

    while (time++ < maxTime) {
        sky.fastForward()
    }

    return
}

const pattern = /position=<\s?(-?\d+),\s+(-?\d+)> velocity=<([-\s]\d+),\s+(-?\d)>/

const lights = new Array()

for (const i in lines) {
    const line = lines[i]
    const results = pattern.exec(line)
    lights.push(new Light(
        parseInt(results[1]),
        parseInt(results[2]),
        parseInt(results[3]),
        parseInt(results[4])
    ))
}

const sky = new Sky(lights)

console.log('Day 10')
console.log('  Part 1 - Watch output in terminal until legible message appears')
console.log('  Part 2 - Printed message includes seconds elapsed')
part1(sky)