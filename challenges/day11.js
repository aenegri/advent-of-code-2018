const part1 = (width, depth, serial) => {
    const powerCache = new Array()
    let maxPowerX = 1
    let maxPowerY = 1
    let maxPower = null

    for (let row = 1; row <= depth - 2; row++) {
        let col = 1
        let currentPower = 0
        
        // shift over one column at a time
        while (col <= width) {
            if (col > 3) {
                // subtract power from old column
                const preCol = col - 3
                for (let rowSquare = row; rowSquare < row + 3; rowSquare++) {
                    currentPower -= powerCache[rowSquare][preCol]
                }
            }

            for (let rowSquare = row; rowSquare < row + 3; rowSquare++) {
                if (!(powerCache[rowSquare] && powerCache[rowSquare][col])) {
                    if (!powerCache[rowSquare]) powerCache[rowSquare] = new Array()

                    powerCache[rowSquare][col] = calculatePower(col, rowSquare, serial)
                }
                currentPower += powerCache[rowSquare][col]
            }

            // update totals
            if (!maxPower || currentPower > maxPower) {
                maxPowerX = col - 2
                maxPowerY = row
                maxPower = currentPower
            }

            col++
        }
    }

    return {x: maxPowerX, y: maxPowerY}
}

const part2 = (width, depth, serial) => {
    const powerCache = new Array()
    let maxPowerX = 1
    let maxPowerY = 1
    let maxPowerSize = 0
    let maxPower = null

    for (let row = 1; row <= depth; row++) {

        for (let col = 1; col <= width; col++) {
            // try every square size that is in bounds
            // with top left corner at (col, row)

            let squareSize = 1
            let currentPower = 0

            while (row + squareSize <= depth + 1) {
                const bottomRow = row + squareSize - 1
                for (let bottomCol = col; bottomCol < col + squareSize; bottomCol++) {
                    currentPower += getPower(bottomCol, bottomRow, serial, powerCache)
                } 

                const rightCol = col + squareSize - 1
                for (let rightRow = row; rightRow < row + squareSize - 1; rightRow++) {
                    currentPower += getPower(rightCol, rightRow, serial, powerCache)
                }

                // update totals
                if (!maxPower || currentPower > maxPower) {
                    maxPowerX = col
                    maxPowerY = row
                    maxPowerSize = squareSize
                    maxPower = currentPower
                }

                squareSize++
            }
        }
    }
    
    return {x: maxPowerX, y: maxPowerY, size: maxPowerSize}
}

const getPower = (x, y, serial, powerCache) => {
    if (!powerCache[x]) powerCache[x] = new Array()

    if (!powerCache[x][y]) powerCache[x][y] = calculatePower(x, y, serial)

    return powerCache[x][y]
}

const calculatePower = (x, y, serial) => {
    const rackId = x + 10
    let power = Math.floor((((rackId * y + serial) * rackId) / 100) % 10) - 5
    return power
}

const serial = 9798
    
const width = 300
const depth = 300

const coordinate1 = part1(width, depth, serial)
const coordinate2 = part2(width, depth, serial)

console.log('Day 11')
console.log(`  Part 1 Solution - (${coordinate1.x}, ${coordinate1.y})`)
console.log(`  Part 1 Solution - (${coordinate2.x}, ${coordinate2.y}, ${coordinate2.size})`)