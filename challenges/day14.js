class Scoreboard {
    constructor(firstScore, secondScore, maxSizeLastScores) {
        this.firstElfNode = new Node(firstScore, null)
        this.secondElfNode = new Node(secondScore, this.firstElfNode)
        this.firstElfNode.next = this.secondElfNode

        this.firstNode = this.firstElfNode
        this.lastNode = this.secondElfNode
        this.lastScores = [firstScore, secondScore]
        this.count = 0
        this.maxSizeLastScores = maxSizeLastScores
    }

    append(score) {
        this.count++

        const node = new Node(score, this.firstNode)
        this.lastNode.next = node
        this.lastNode = node

        if (this.lastScores.length >= this.maxSizeLastScores) this.lastScores.shift()
        
        this.lastScores.push(score)
    }

    moveElves() {
        let firstElfMoves = this.firstElfNode.score + 1
        let secondElfMoves = this.secondElfNode.score + 1

        while (firstElfMoves > 0) {
            this.firstElfNode = this.firstElfNode.next
            firstElfMoves--
        }

        while (secondElfMoves > 0) {
            this.secondElfNode = this.secondElfNode.next
            secondElfMoves--
        }
    }

    getCombinedScore() {
        return this.firstElfNode.score + this.secondElfNode.score
    }

    getLastScores() {
        return this.lastScores
    }
}

class Node {
    constructor(score, next) {
        this.score = score
        this.next = next
    }
}

const arrayToString = (array) => {
    let answer = ''
    for (const i in array) {
        answer += array[i]
    }
    return answer
}

const atScoreSequence = (scoreboard, sequence) => {
    let seq = arrayToString(scoreboard.getLastScores())
    return seq === sequence
}

const part1 = (recipesCount) => {
    const targetRecipes = recipesCount + 10
    const scoreboard = new Scoreboard(3, 7, 10)
    let count = 2 // start with two recipes 3 and 7

    while (count < targetRecipes) {
        const sum = scoreboard.getCombinedScore()

        if (sum > 9) {
            const firstScore = 1
            const secondScore = sum - 10
            scoreboard.append(firstScore)
            scoreboard.append(secondScore)
            count += 2
        }
        else {
            scoreboard.append(sum)
            count++
        }

        scoreboard.moveElves()
    }

    
    const nextScores = scoreboard.getLastScores()

    return arrayToString(nextScores)
}

// Rather slow but don't want to spend a ton
// of time optimizing. To improve speed,
// keep a string that stores the current pattern
// and modify it instead of regenerating
// the string after each recipe
const part2 = (scoreSequence) => {
    scoreSequence = scoreSequence.toString()
    let count = 2 - scoreSequence.length // count *before* the target sequence
    const scoreboard = new Scoreboard(3, 7, scoreSequence.length)

    

    while (true) {
        const sum = scoreboard.getCombinedScore()

        if (sum > 9) {
            const firstScore = 1
            const secondScore = sum - 10
            scoreboard.append(firstScore)
            count++
            if (atScoreSequence(scoreboard, scoreSequence)) return count
            scoreboard.append(secondScore)
            count++
            if (atScoreSequence(scoreboard, scoreSequence)) return count
        }
        else {
            scoreboard.append(sum)
            count++
            if (atScoreSequence(scoreboard, scoreSequence)) return count
        }

        scoreboard.moveElves()
    }
}

const input = 824501

console.log('Day 14')
console.log(`  Part 1 Solution - ${part1(input)}`)
console.log(`  Part 2 Solution - ${part2(input)}`)