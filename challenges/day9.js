const fs = require('fs')
const path = require('path')

const file = fs.readFileSync(path.resolve(__dirname, 'input', 'day9.txt'))

const pattern = /(\d+) players; last marble is worth (\d+) points/
const results = pattern.exec(file.toString())
const playerCount = parseInt(results[1])
const lastMarble = parseInt(results[2])

const part1 = (playerCount, lastMarble) => {
    const circle = new Circle()
    const idToScore = {}
    let currentIndex = 0

    for (let i = 1; i <= playerCount; i++) {
        idToScore[i] = 0
    }

    for (let i = 1; i <= lastMarble; i++) {
        if (i % 23 === 0) {
            let pointsScored = circle.remove() + i
            let playerId = i % playerCount + 1
            idToScore[playerId] += pointsScored
        }
        else {
            circle.add(i)
        }
    }

    let highScore = 0
    for (const id in idToScore) {
        highScore = Math.max(highScore, idToScore[id])
    }

    return highScore
}

// Essentially a custom linked list
// for this problem
class Circle {
    constructor() {
        // start with 0 marble linked to itself
        this.currentMarble = new Marble(0, null, null)
        this.currentMarble.prev = this.currentMarble
        this.currentMarble.next = this.currentMarble

        this.size = 1
    }

    add(value) {
        const preInsert = this.currentMarble.next
        const postInsert = preInsert.next
        
        const marble = new Marble(value, preInsert, postInsert)
        preInsert.next = marble
        postInsert.prev = marble
        this.currentMarble = marble

        this.size++
    }

    remove() {
        let marble = this.currentMarble
        for (let i = 0; i < 7; i++) {
            marble = marble.prev
        }
        let preRemove = marble.prev
        let postRemove = marble.next

        preRemove.next = postRemove
        postRemove.prev = preRemove

        this.currentMarble = postRemove

        this.size--

        return marble.value
    }

    toString() {
        let marble = this.currentMarble
        let line = `(${marble.value})<-->`
        for (let i = 0; i < this.size; i++) {
            marble = marble.next
            line += marble.value + '<->'
        }
        return line.slice(0, -4)
    }
}

class Marble {
    constructor(value, prev, next) {
        this.value = value
        this.prev = prev
        this.next = next
    }
}

console.log('Day 9')
console.log(`  Part 1 Solution - ${part1(playerCount, lastMarble)}`)
console.log(`  Part 2 Solution - ${part1(playerCount, lastMarble * 100)}`)