const fs = require('fs')
const path = require('path')
const file = fs.readFileSync(path.resolve(__dirname, 'input', 'day12.txt'))

const lines = file.toString().split('\n')

const genZeroRegex = /^initial state:\s(\S+)/
const ruleRegex = /(\S{5})\s=>\s(#|\.)$/

const genZeroString = genZeroRegex.exec(lines[0])[1]
const genZero = new Array()
const rules = new Set()

for (let i = 0; i < genZeroString.length; i++) {
    genZero.push(genZeroString.charAt(i))
}

for (let i = 2; i < lines.length; i++) {
    const results = ruleRegex.exec(lines[i])
    if (results[2] === '#') rules.add(results[1])
}

const part1 = (genZero, rules, cycles) => {
    let genNow = genZero
    let genNext = new Array()

    let firstPlant = 0
    let lastPlant = genZero.length

    for (let c = 1; c <= cycles; c++) {
        let start = firstPlant - 2
        let end = lastPlant + 3


        for (let i = start; i < end; i++) {
            let window = getWindow(i - 2, i + 2, genNow)

            if (rules.has(window)) {
                genNext[i] = '#'
                firstPlant = Math.min(firstPlant, i)
                lastPlant = Math.max(lastPlant, i)
            }
            else {
                genNext[i] = '.'
            }
        }

        genNow = genNext
        genNext = new Array()
    }
    
    return getTotal(genNow)
}

// Solving for such a large value would take
// much too long. Instead, look for a pattern.
// Eventually, the delta between generations
// stabilizes
const part2 = (genZero, rules, cycles) => {
    let genNow = genZero
    let genNext = new Array()

    let firstPlant = 0
    let lastPlant = genZero.length

    let lastTotal = 0
    let currentTotal = 0
    let lastDelta = 0
    let currentDelta = 0
    let sameDeltaCount = 0
    let answer

    for (let c = 1; c <= cycles; c++) {
        let start = firstPlant - 2
        let end = lastPlant + 3

        for (let i = start; i < end; i++) {
            let window = getWindow(i - 2, i + 2, genNow)

            if (rules.has(window)) {
                genNext[i] = '#'
                firstPlant = Math.min(firstPlant, i)
                lastPlant = Math.max(lastPlant, i)
            }
            else {
                genNext[i] = '.'
            }
        }

        genNow = genNext
        genNext = new Array()

        lastTotal = currentTotal
        currentTotal = getTotal(genNow)
        currentDelta = currentTotal - lastTotal

        if (currentDelta === lastDelta) {
            sameDeltaCount++
        }
        else {
            sameDeltaCount = 0
        }

        if (sameDeltaCount >10) {
            let cyclesLeft = 50000000000 - c
            answer = currentTotal + (cyclesLeft * currentDelta)
            break
        }

        lastDelta = currentDelta
    }
    
    return answer
}

const getWindow = (start, end, gen) => {
    let plants = ''
    for (let i = start; i <= end; i++) {
        plants += !gen[i] ? '.' : gen[i]
    }
    return plants
}

const getTotal = (gen) => {
    let total = 0
    for(const i in gen) {
        if (gen[i] === '#') total += parseInt(i)
    }

    return total
}

console.log('Day 12')
console.log(`  Part 1 Solution - ${part1(genZero, rules, 20)}`)
console.log(`  Part 2 Solution - ${part2(genZero, rules, 50000000000)}`)