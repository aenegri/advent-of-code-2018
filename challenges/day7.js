const fs = require('fs')
const path = require('path')
const PriorityQueue = require('priorityqueue')

const file = fs.readFileSync(path.resolve(__dirname, 'input', 'day7.txt'))

const lines = file.toString().split('\n')

const part1 = (lines) => {
    const pattern = /^Step (\w) must be finished before step (\w) can begin\.$/

    const nodeMap = {} // id -> node object
    const pq = new PriorityQueue({
        comparator: (a, b) => {
            return b.id.charCodeAt(0) - a.id.charCodeAt(0)
        }
    })
    let path = ''

    // setup graph
    for (const i in lines) {
        const line = lines[i]
        const results = pattern.exec(line)
        const parentId = results[1]
        const childId = results[2]

        if (!nodeMap[parentId]) nodeMap[parentId] = new Node(parentId)
        if (!nodeMap[childId]) nodeMap[childId] = new Node(childId)

        nodeMap[parentId].children.push(childId)
        nodeMap[childId].parentCount++
    }

    // find root nodes
    for (const id in nodeMap) {
        if (nodeMap[id].parentCount === 0) pq.push(nodeMap[id])
    }

    // process graph
    while (!pq.empty()) {
        let currentNode = pq.pop()

        path += currentNode.id
        currentNode.visited = true
        const children = currentNode.children

        for (const i in children) {
            const childNode = nodeMap[children[i]]

            if (!childNode.visited) {
                childNode.parentCount--

                if (childNode.parentCount === 0) {
                    pq.push(childNode)
                }
            }
        }
    }

    return path
}

class Node {
    constructor(id) {
        this.id = id
        this.children = new Array()
        this.visited = false
        this.parentCount = 0
    }
}

const part2 = (lines) => {
    const pattern = /^Step (\w) must be finished before step (\w) can begin\.$/

    const nodeMap = {} // id -> node object
    const pq = new PriorityQueue({
        comparator: (a, b) => {
            return b.id.charCodeAt(0) - a.id.charCodeAt(0)
        }
    })
    let path = ''

    // setup graph
    for (const i in lines) {
        const line = lines[i]
        const results = pattern.exec(line)
        const parentId = results[1]
        const childId = results[2]

        if (!nodeMap[parentId]) nodeMap[parentId] = new Node(parentId)
        if (!nodeMap[childId]) nodeMap[childId] = new Node(childId)

        nodeMap[parentId].children.push(childId)
        nodeMap[childId].parentCount++
    }

    // find root nodes
    for (const id in nodeMap) {
        if (nodeMap[id].parentCount === 0) pq.push(nodeMap[id])
    }

    const pool = new WorkerPool(5)
    let second = 0 // elapsed time

    // iterate as long as the pool and the queue are not both empty
    while (!pq.empty() || !pool.empty()) {
        // keep as busy as possible
        while (!pq.empty() && pool.hasRoom()) {
            pool.addProject(pq.pop().id)
        }

        const completedIds = pool.work()
        second++

        // now access children of completed nodes
        for (const i in completedIds) {
            const currentNode = nodeMap[completedIds[i]]

            currentNode.visited = true
            const children = currentNode.children

            for (const i in children) {
                const childNode = nodeMap[children[i]]

                if (!childNode.visited) {
                    childNode.parentCount--

                    if (childNode.parentCount === 0) {
                        pq.push(childNode)
                    }
                }
            }
        }
    }

    return second
}

class WorkerPool {
    constructor(capacity) {
        this.capacity = capacity
        this.busyCount = 0
        this.projects = {} // id -> seconds remaining
    }

    hasRoom() {
        return this.busyCount < this.capacity
    }

    empty() {
        return this.busyCount === 0
    }

    addProject(id) {
        this.busyCount++
        this.projects[id] = id.charCodeAt(0) - 4
    }

    work() {
        const completedProjects = new Array()
        const projects = this.projects

        for (const id in projects) {
            if (--projects[id] === 0) {
                this.busyCount--
                completedProjects.push(id)
                delete projects[id]
            }
        }

        return completedProjects
    }
}

console.log('Day 7')
console.log(`  Part 1 Solution - ${part1(lines)}`)
console.log(`  Part 2 Solution - ${part2(lines)}`)