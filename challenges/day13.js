const fs = require('fs')
const path = require('path')
const file = fs.readFileSync(path.resolve(__dirname, 'input', 'day13.txt'))

const lines = file.toString().split('\n')

const track = new Array()
const cartSymbols = new Set(['^', '>', '<', 'v'])
const cartToTrack = {
    '^': '|',
    'v': '|',
    '>': '-',
    '<': '-'
}

const carts = new Array()
const carts2 = new Array()


for (const row in lines) {
    const line = lines[row]

    for (let col = 0; col < line.length; col++) {
        if (!track[col]) track[col] = new Array()
        const c = line.charAt(col)
        if (cartSymbols.has(c)) {
            carts.push({
                x: col,
                y: row,
                dir: c,
                turns: 0
            })
            carts2.push({
                x: col,
                y: row,
                dir: c,
                turns: 0
            })
            track[col][row] = cartToTrack[c]
        }
        else {
            //console.log(c)
            track[col][row] = c
        }
    }
}

// Note: Code may look ugly.
// Attempting to complete for speed and
// plan to not make it look nice in order
// to show what it actually looked like
const part1 = (track, carts) => {
    const cartCoords = {}
    while (true) {
        carts.sort((a, b) => {
            if (a.x === b.x) return a.y - b.y
            else return a.x - b.x
        })

        // each cart makes movement
        for (const i in carts) {
            const cart = carts[i]

            // based on current direction,
            // 1) get next part of track
            // 2) update pos

            let nextTrack
            const lastCoord = `${cart.x},${cart.y}`

            if (cart.dir === '^') {
                cart.y--
                nextTrack = track[cart.x][cart.y]
            }
            else if (cart.dir === 'v') {
                cart.y++
                nextTrack = track[cart.x][cart.y]
            }
            else if (cart.dir === '>') {
                cart.x++
                nextTrack = track[cart.x][cart.y]
            }
            else if (cart.dir === '<') {
                cart.x--
                nextTrack = track[cart.x][cart.y]
            }
            else {
                console.log('BAD DIRECTION:', cart.dir)
            }

            //console.log('Direction:', cart.dir)
            //console.log('Next tracK', nextTrack)

            // check for collision
            const coord = `${cart.x},${cart.y}`
            if (cartCoords[coord]) {
                // collided
                return coord
            }
            else {
                // cleanup last pos
                delete cartCoords[lastCoord]
                cartCoords[coord] = true
            }

            // based on next track,
            // 1) change direction
            // 2) update turns if at intersection
            //console.log('\nPos', coord)
            //console.log('Original Direction:', cart.dir)
            //console.log('Next Track:', nextTrack)
            //console.log('Turn Count:', cart.turns)
            if (nextTrack === '\\') {
                //console.log('test')
                if (cart.dir === '^') cart.dir = '<'
                else if (cart.dir === '<') cart.dir = '^'
                else if (cart.dir === 'v') cart.dir = '>'
                else if (cart.dir === '>') cart.dir = 'v'
            }
            else if (nextTrack === '/') {
                //console.log('test2')
                if (cart.dir === '^') cart.dir = '>'
                else if (cart.dir === '<') cart.dir = 'v'
                else if (cart.dir === 'v') cart.dir = '<'
                else if (cart.dir === '>') cart.dir = '^'
            }
            else if (nextTrack === '+') {
                if (cart.turns % 3 === 0) {
                    if (cart.dir === '^') cart.dir = '<'
                    else if (cart.dir === '<') cart.dir = 'v'
                    else if (cart.dir === 'v') cart.dir = '>'
                    else if (cart.dir === '>') cart.dir = '^'
                }
                else if (cart.turns % 3 === 1) {
                    // straight
                }
                else if (cart.turns % 3 === 2) {
                    if (cart.dir === '^') cart.dir = '>'
                    else if (cart.dir === '<') cart.dir = '^'
                    else if (cart.dir === 'v') cart.dir = '<'
                    else if (cart.dir === '>') cart.dir = 'v'
                }
                cart.turns++
            }
            else if (nextTrack === '-' || nextTrack === '|') {
                // continue that direction
            }
            else {
                console.log('INVALID TRACK ENCOUNTERED:', nextTrack)
            }

            //console.log('New direction:', cart.dir)
        }
    }
}

const part2 = (track, carts) => {
    const cartCoords = {}
    while (Object.keys(carts).length > 1) {
        carts.sort((a, b) => {
            if (a.x === b.x) return a.y - b.y
            else return a.x - b.x
        })

        // each cart makes movement
        for (const i in carts) {
            const cart = carts[i]

            // based on current direction,
            // 1) get next part of track
            // 2) update pos

            let nextTrack
            const lastCoord = `${cart.x},${cart.y}`

            if (cart.dir === '^') {
                cart.y--
                nextTrack = track[cart.x][cart.y]
            }
            else if (cart.dir === 'v') {
                cart.y++
                nextTrack = track[cart.x][cart.y]
            }
            else if (cart.dir === '>') {
                cart.x++
                nextTrack = track[cart.x][cart.y]
            }
            else if (cart.dir === '<') {
                cart.x--
                nextTrack = track[cart.x][cart.y]
            }
            else {
                console.log('BAD DIRECTION:', cart.dir)
            }

            //console.log('Direction:', cart.dir)
            //console.log('Next tracK', nextTrack)

            // check for collision
            const coord = `${cart.x},${cart.y}`
            if (cartCoords[coord]) {
                // collided
                const otherCartIndex = carts.findIndex((c, idx) => {
                    if (!c) return false
                    if (i == idx) return false
                    return c.x == cart.x && c.y == cart.y
                })
                //console.log('Collision at:', coord, 'with cart', i, 'and', otherCartIndex)

                delete carts[otherCartIndex]
                delete carts[i]
                delete cartCoords[coord]
                //console.log(Object.keys(carts).length)
                delete cartCoords[lastCoord]
                continue
            }
            else {
                cartCoords[coord] = true
            }
            delete cartCoords[lastCoord]

            // based on next track,
            // 1) change direction
            // 2) update turns if at intersection
            //console.log('\nPos', coord)
            //console.log('Original Direction:', cart.dir)
            //console.log('Next Track:', nextTrack)
            //console.log('Turn Count:', cart.turns)
            if (nextTrack === '\\') {
                //console.log('test')
                if (cart.dir === '^') cart.dir = '<'
                else if (cart.dir === '<') cart.dir = '^'
                else if (cart.dir === 'v') cart.dir = '>'
                else if (cart.dir === '>') cart.dir = 'v'
            }
            else if (nextTrack === '/') {
                //console.log('test2')
                if (cart.dir === '^') cart.dir = '>'
                else if (cart.dir === '<') cart.dir = 'v'
                else if (cart.dir === 'v') cart.dir = '<'
                else if (cart.dir === '>') cart.dir = '^'
            }
            else if (nextTrack === '+') {
                if (cart.turns % 3 === 0) {
                    if (cart.dir === '^') cart.dir = '<'
                    else if (cart.dir === '<') cart.dir = 'v'
                    else if (cart.dir === 'v') cart.dir = '>'
                    else if (cart.dir === '>') cart.dir = '^'
                }
                else if (cart.turns % 3 === 1) {
                    // straight
                }
                else if (cart.turns % 3 === 2) {
                    if (cart.dir === '^') cart.dir = '>'
                    else if (cart.dir === '<') cart.dir = '^'
                    else if (cart.dir === 'v') cart.dir = '<'
                    else if (cart.dir === '>') cart.dir = 'v'
                }
                cart.turns++
            }
            else if (nextTrack === '-' || nextTrack === '|') {
                // continue that direction
            }
            else {
                console.log('INVALID TRACK ENCOUNTERED:', nextTrack)
            }

            //console.log('New direction:', cart.dir)
        }
    }
    const id = Object.keys(carts)[0]
    const cart = carts[id]

    return `${cart.x},${cart.y}`
}


const part1Solution = part1(track, carts)
const part2Solution = part2(track, carts2)

console.log('Day 13')
console.log(`  Part 1 Solution - ${part1Solution}`)
console.log(`  Part 2 Solution - ${part2Solution}`)