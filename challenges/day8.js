const fs = require('fs')
const path = require('path')

const file = fs.readFileSync(path.resolve(__dirname, 'input', 'day8.txt'))

const nums = file.toString().split(' ').map(value => parseInt(value))
const nums1 = nums.slice()
const nums2 = nums.slice()

const part1 = (nums) => {
    const childrenCount = nums.shift()
    const metadataCount = nums.shift()
    let sum = 0

    for (let i = 0; i < childrenCount; i++) {
        sum += part1(nums)
    }

    for (let i = 0; i < metadataCount; i++) {
        sum += nums.shift()
    }

    return sum
}

const part2 = (nums) => {
    const childrenCount = nums.shift()
    const metadataCount = nums.shift()
    let sum = 0

    if (childrenCount) {
        const childValues = new Array()
        for (let i = 0; i < childrenCount; i++) {
            childValues.push(part2(nums))
        }

        for (let i = 0; i < metadataCount; i++) {
            const targetChild = nums.shift() - 1 // adjust to be 0 based

            if (targetChild >= 0 && targetChild < childValues.length) sum += childValues[targetChild]
        }
    }
    else {
        for (let i = 0; i < metadataCount; i++) {
            sum += nums.shift()
        }
    }

    return sum
}

console.log('Day 8')
console.log(`  Part 1 Solution - ${part1(nums1)}`)
console.log(`  Part 2 Solution - ${part2(nums2)}`)