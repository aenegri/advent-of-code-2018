const fs = require('fs')
const path = require('path')
const file = fs.readFileSync(path.resolve(__dirname, 'input', 'day3.txt'))

const lines = file.toString().split('\n')

const part1 = (lines) => {
    const re = /^#\d+\s@\s(\d+),(\d+):\s(\d+)x(\d+)$/
    const overlapMap = new Array()

    let overlapCount = 0

    for (const i in lines) {
        const line = lines[i]
        const results = re.exec(line)
        const x = parseInt(results[1])
        const y = parseInt(results[2])
        const width = parseInt(results[3])
        const length = parseInt(results[4])
        const rightEdge = x + width
        const bottomEdge = y + length

        for (let j = x; j < rightEdge; j++) {
            if (!overlapMap[j]) overlapMap[j] = new Array()

            for (let k = y; k < bottomEdge; k++) {
                if (overlapMap[j][k] === 1) {
                    overlapMap[j][k] = 2
                    overlapCount++
                }
                else if (!overlapMap[j][k]) {
                    overlapMap[j][k] = 1
                }
            }
        }
    }

    return overlapCount
}

const part2 = (lines) => {
    const re = /^#(\d+)\s@\s(\d+),(\d+):\s(\d+)x(\d+)$/
    const isolatedIds = new Set()
    const overlapMap = new Array()

    for (const i in lines) {
        const line = lines[i]
        const results = re.exec(line)
        const id = parseInt(results[1])
        const x = parseInt(results[2])
        const y = parseInt(results[3])
        const width = parseInt(results[4])
        const length = parseInt(results[5])
        const rightEdge = x + width
        const bottomEdge = y + length
        let collision = false

        for (let j = x; j < rightEdge; j++) {
            if (!overlapMap[j]) overlapMap[j] = new Array()

            for (let k = y; k < bottomEdge; k++) {
                if (overlapMap[j][k]) {
                    isolatedIds.delete(overlapMap[j][k])
                    collision = true
                }
                else if (!overlapMap[j][k]) {
                    overlapMap[j][k] = id
                }
            }
        }

        if (!collision) isolatedIds.add(id)
    }

    return isolatedIds.values().next().value
}

console.log('Day 3')
console.log(`  Part 1 Solution - ${part1(lines)}`)
console.log(`  Part 2 Solution - ${part2(lines)}`)