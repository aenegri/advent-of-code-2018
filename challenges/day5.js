const fs = require('fs')
const path = require('path')
const file = fs.readFileSync(path.resolve(__dirname, 'input', 'day5.txt'))

const polymer = file.toString()

const part1 = (polymer) => {
    let i = 0
    
    while (i + 1 < polymer.length) {
        const currentAscii = polymer.charCodeAt(i)
        const nextAscii = polymer.charCodeAt(i+1)

        if (Math.abs(currentAscii - nextAscii) === 32) {
            // opposite polarity
            polymer = polymer.substring(0,i) + polymer.substring(i+2)

            // decrement index unless already at 0
            i = i > 0 ? i - 1 : i 
        }
        else {
            i++
        }
    }

    return polymer.length
}

const part2 = (polymer) => {
    const firstUnit = 65
    const lastUnit = 90
    let shortestPolymer = Number.MAX_SAFE_INTEGER

    for (let i = firstUnit; i <= lastUnit; i++) {
        const regex = new RegExp(`${String.fromCharCode(i)}|${String.fromCharCode(i+32)}`, 'g')
        let revisedPolymer = polymer.replace(regex, '')

        let j = 0

        while (j + 1 < revisedPolymer.length) {
            const currentAscii = revisedPolymer.charCodeAt(j)
            const nextAscii = revisedPolymer.charCodeAt(j+1)
    
            if (Math.abs(currentAscii - nextAscii) === 32) {
                // opposite polarity
                revisedPolymer = revisedPolymer.substring(0,j) + revisedPolymer.substring(j+2)
    
                // decrement index unless already at 0
                j = j > 0 ? j - 1 : j
            }
            else {
                j++
            }
        }

        shortestPolymer = Math.min(revisedPolymer.length, shortestPolymer)
    }
    

    return shortestPolymer
}

console.log('Day 5')
console.log(`  Part 1 Solution - ${part1(polymer)}`)
console.log(`  Part 2 Solution - ${part2(polymer)}`)