const fs = require('fs')
const path = require('path')
const file = fs.readFileSync(path.resolve(__dirname, 'input', 'day2.txt'))

const lines = file.toString().split('\n')

const part1 = (lines) => {
    let twoLetterCount = 0
    let threeLetterCount = 0

    for (const index in lines) {
        const line = lines[index]
        const freqMap = {}

        let lineTwoLetterCount = 0
        let lineThreeLetterCount = 0

        for (let i = 0; i < line.length; i++) {
            const c = line.charAt(i)

            if (freqMap[c] === 1) {
                lineTwoLetterCount++
            }
            else if (freqMap[c] === 2) {
                lineTwoLetterCount--
                lineThreeLetterCount++
            }
            else if (freqMap[c] === 3) {
                lineThreeLetterCount--
            }
            else if (!freqMap.hasOwnProperty(c)) {
                freqMap[c] = 0
            }

            freqMap[c] = freqMap[c] + 1
        }
        
        if (lineTwoLetterCount > 0) twoLetterCount++
        if (lineThreeLetterCount > 0) threeLetterCount++
    }

    return twoLetterCount * threeLetterCount
}

const part2 = (lines) => {
    const seenSet = {}

    for (const i in lines) {
        const line = lines[i]

        for (let j = 0; j < line.length; j++) {
            const c = line.charAt(j)
            const candidate = line.substring(0, j) + '?' + line.substring(j + 1)

            if (seenSet.hasOwnProperty(candidate)) {
                return candidate.replace('?', '')
            }

            seenSet[candidate] = true
        }
    }
}

console.log('Day 2')
console.log(`  Part 1 Solution - ${part1(lines)}`)
console.log(`  Part 2 Solution - ${part2(lines)}`)