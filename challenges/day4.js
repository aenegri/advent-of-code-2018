const fs = require('fs')
const path = require('path')
const file = fs.readFileSync(path.resolve(__dirname, 'input', 'day4.txt'))

const lines = file.toString().split('\n')

const timePattern = /^\[(\d{4})-(\d{2})-(\d{2})\s(\d{2}):(\d{2})\]/
const sortedLines = lines.slice().sort((a, b) => {
    const resultsA = timePattern.exec(a)
    const resultsB = timePattern.exec(b)

    const dateA = new Date(
        resultsA[1],
        resultsA[2] - 1,
        resultsA[3],
        resultsA[4],
        resultsA[5]
    )
    const dateB = new Date(
        resultsB[1],
        resultsB[2] - 1,
        resultsB[3],
        resultsB[4],
        resultsB[5]
    )

    return dateA - dateB
})

const part1 = (lines) => {
    const idPattern = /#(\d+)/

    const idToMinuteCount = {}
    let id = null
    let awake = true
    let lastAwakeMinute = 0
    let sleepiestGuard = null

    const incrementRange = (id, start, end) => {
        if (!id) return

        if (!idToMinuteCount[id]) {
            idToMinuteCount[id] = {
                sum: 0,
                counts: new Array()
            }
        }

        for (let i = start; i < end; i++) {
            idToMinuteCount[id].sum = idToMinuteCount[id].sum + 1
            
            if (idToMinuteCount[id].counts[i]) {
                idToMinuteCount[id].counts[i] = idToMinuteCount[id].counts[i] + 1
            }
            else {
                idToMinuteCount[id].counts[i] = 1
            }
        }

        if (sleepiestGuard === null) sleepiestGuard = id
        if (idToMinuteCount[id].sum > idToMinuteCount[sleepiestGuard].sum) sleepiestGuard = id
    }

    for (const i in lines) {
        const line = lines[i]
        const idResults = idPattern.exec(line)
        const timeResults = timePattern.exec(line)
        const minute = parseInt(timeResults[5])

        if (idResults) {
            // end of previous guard duty
            id = parseInt(idResults[1])
            awake = true
        }
        else if (awake) {
            // falling asleep
            lastAwakeMinute = minute
            awake = false
        }
        else {
            // waking up
            incrementRange(id, lastAwakeMinute, minute)
            awake = true
        }
    }

    let sleepiestMinute = null
    for (const i in idToMinuteCount[sleepiestGuard].counts) {
        const count = idToMinuteCount[sleepiestGuard].counts[i]

        if (sleepiestMinute === null) sleepiestMinute = i
        if (count > idToMinuteCount[sleepiestGuard].counts[sleepiestMinute]) {
            sleepiestMinute = i
        }
    }

    return sleepiestMinute * sleepiestGuard
}

const part2 = (lines) => {
    const idPattern = /#(\d+)/

    const idMinuteToCount = {}
    let id = null
    let awake = true
    let lastAwakeMinute = 0
    let sleepiestGuard = null
    let sleepiestMinute = null

    const incrementRange = (id, start, end) => {
        if (!id) return

        if (!idMinuteToCount[id]) {
            idMinuteToCount[id] = new Array()
        }

        for (let i = start; i < end; i++) {
            
            if (idMinuteToCount[id][i]) {
                idMinuteToCount[id][i] = idMinuteToCount[id][i] + 1
            }
            else {
                idMinuteToCount[id][i] = 1
            }

            // check if max has changed
            if (sleepiestGuard === null || idMinuteToCount[id][i] > idMinuteToCount[sleepiestGuard][sleepiestMinute]) {
                sleepiestGuard = id
                sleepiestMinute = i
            }
        }
    }

    for (const i in lines) {
        const line = lines[i]
        const idResults = idPattern.exec(line)
        const timeResults = timePattern.exec(line)
        const minute = parseInt(timeResults[5])

        if (idResults) {
            // end of previous guard duty
            id = parseInt(idResults[1])
            awake = true
        }
        else if (awake) {
            // falling asleep
            lastAwakeMinute = minute
            awake = false
        }
        else {
            // waking up
            incrementRange(id, lastAwakeMinute, minute)
            awake = true
        }
    }

    return sleepiestMinute * sleepiestGuard
}

console.log('Day 4')
console.log(`  Part 1 Solution - ${part1(sortedLines)}`)
console.log(`  Part 2 Solution - ${part2(sortedLines)}`)